<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cleared!";
});
Route::get('/','PageController@index')->name('welcome');
Route::get('/contact','PageController@contact')->name('contact');
Route::get('/company','PageController@company')->name('developer');
Route::get('/company/search','PageController@searchCompany')->name('company.search');
Route::get('/researcher','PageController@researcher')->name('researcher');
Route::get('/researcher/search','PageController@searchResearcher')->name('search.researcher');
Route::get('/profile','PageController@profile')->name('profile');
Route::get('/about','PageController@about')->name('about');
Route::get('/company/details{id}','PageController@companyDetails')->name('company.details');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/video', 'MessageController@video')->name('video');
Route::post('/pusher/auth', 'MessageController@authenticate');

Route::get('/call/request/{id}', 'MessageController@callRequest');
Route::get('/caller/page{id}', 'MessageController@callerPage');
Route::group(['prefix'=>'user','middleware'=>'auth','namespace'=>'User'], function(){

  	Route::get('dashboard','DashboardController@index')->name('user.dashboard');
	Route::get('profile/edit','ProfileController@edit')->name('user.edit');
	Route::get('profile/view','ProfileController@index')->name('user.index');
	Route::get('profile/detail','ProfileController@detail')->name('user.detail');
	Route::get('profile/education','EducationController@index')->name('user.education');
	Route::get('profile/edushow','EducationController@show')->name('user.edushow');
    Route::get('profile/educationdetail','EducationController@detail')->name('user.educationdetail');
	Route::get('profile/research','ResearchController@index')->name('user.research');
	Route::get('profile/researchshow','ResearchController@show')->name('user.researchshow');
    Route::get('profile/researchdetail','ResearchController@detail')->name('user.researchdetail');
	Route::get('profile/achievement','AchievementController@index')->name('user.achievement');
	Route::get('profile/achievementshow','AchievementController@show')->name('user.achievementshow');
    Route::get('profile/achievementdetail','AchievementController@detail')->name('user.achievementdetail');
	Route::get('profile/skill','SkillController@index')->name('user.skill');
	Route::get('profile/skillshow','SkillController@show')->name('user.skillshow');
    Route::get('profile/skilldetail','SkillController@detail')->name('user.skilldetail');
    Route::post('profile/skill/store','SkillController@store')->name('user.skill.store');
	Route::get('profile/work','WorkController@index')->name('user.work');
    Route::post('bid','BidController@store')->name('user.bid');
});
Route::get('/message', 'MessageController@index')->name('message');
Route::get('/contacts','MessageController@getContacts');
Route::get('/get/message/{id}','MessageController@getMessage');
Route::post('/sent/message','MessageController@sentMessage');

Route::group(['prefix'=>'admin','middleware'=>'auth','namespace'=>'Admin'], function(){
    Route::get('dashboard','DashboardController@index')->name('admin.dashboard');
    Route::get('university','UniversityController@index')->name('university.index');
    Route::get('university/create','UniversityController@create')->name('university.create');
    Route::post('university/store','UniversityController@store')->name('university.store');
    Route::delete('university/destroy{id}','UniversityController@destroy')->name('university.destroy');
    Route::get('degree','DegreeController@index')->name('degree.index');
    Route::get('degree/create','DegreeController@create')->name('degree.create');
    Route::post('degree/store','DegreeController@store')->name('degree.store');
    Route::delete('degree/destroy{id}','DegreeController@destroy')->name('degree.destroy');
    Route::get('tag','TagController@index')->name('tag.index');
    Route::get('tag/create','TagController@create')->name('tag.create');
    Route::post('tag/store','TagController@store')->name('tag.store');
    Route::delete('tag/destroy/{id}','TagController@destroy')->name('tag.destroy');
    Route::resource('account','AccountController');
});
Route::group(['prefix'=>'company','middleware'=>['auth','company'],'namespace'=>'Company'], function(){
    Route::post('work/store','CompanyController@store')->name('work.store');
    Route::get('profile','CompanyController@index')->name('user.company');
    Route::get('work/history','CompanyController@history')->name('company.history');
});
