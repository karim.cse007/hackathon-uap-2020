@extends('layouts.frontend.app')

@section('title','Home')

@push('css')

@endpush('css')

@section('content')
    <div class="second-search-result-wrapper">

        <div class="container">
        </div>
    </div>

    <!-- start hero-header -->
    <div class="breadcrumb-wrapper">

        <div class="container">

            <ol class="breadcrumb-list booking-step">
                <li><a href="#">Developer</a></li>
                <li><span>all developers</span></li>
            </ol>

        </div>

    </div>
    <!-- end hero-header -->
    <div class="second-search-result-wrapper">

        <div class="container">

            <form action="{{route('company.search')}}" method="GET">
                @csrf
                <div class="second-search-result-inner">
                    <span class="labeling">Search <br> Company</span>
                    <div class="row">

                        <div class="col-sm-4 col-md-5">
                            <div class="form-group form-lg">
                                <input type="text" class="form-control" name="search" placeholder="Name: Ex: Company" />
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-2">
                            <button type="submit" class="btn btn-block">Search</button>
                        </div>

                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="section sm">

        <div class="container">

            <div class="employee-grid-wrapper">

                <div class="GridLex-gap-15-wrappper">

                    <div class="GridLex-grid-noGutter-equalHeight">
                        @foreach($companies as $company)
                            <div class="GridLex-col-3_sm-4_xs-6_xss-12">
                                <div class="employee-grid-item">

                                    <div class="action">

                                        <div class="row gap-10">

                                            <div class="col-xs-6 col-sm-6">
                                                <div class="text-left">
                                                    <button class="btn"><i class="icon-heart"></i></button>
                                                </div>
                                            </div>

                                            <div class="col-xs-6 col-sm-6">
                                                <div class="text-right">
                                                    <a class="btn text-right" href="{{ route('company.details',$company->id)}}"><i class="icon-action-redo"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{ route('company.details',$company->id)}}" class="clearfix">

                                        <div class="image clearfix">

                                            <img src="{{asset('frontend/images/2.jpg')}}" alt="Image" class="img-circle" >

                                        </div>

                                        <div class="content">

                                            <h4>{{$company->name}}</h4>
                                            <p class="location">{{$company->addess}}</p>

                                            <h6 class="text-primary">Skills:</h6>

                                            <ul class="employee-skill clearfix">
                                                <li><span>Machine Learning</span></li>
                                                <li><span>Computer vision</span></li>
                                                <li><span>CNN</span></li>
                                                <li><span>LSTM</span></li>
                                            </ul>

                                        </div>

                                    </a>

                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>

            </div>

            <div class="pager-wrapper">

            </div>

        </div>

    </div>
@endsection

@push('scripts')

@endpush
