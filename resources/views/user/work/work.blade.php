@extends('layouts.app')

@section('title','Work History')

@push('css')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">

@endpush

@section('content')



  <div class="GridLex-col-9_sm-8_xs-12">

    <div class="admin-content-wrapper">

   <div class="admin-section-title">
          <div class="row">
            <div class="col-md-12">
               <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">All Work History</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="table" class="table" style="width:100%">
                      <thead class=" text-primary">
                        <th>
                          ID
                        </th>
                        <th>
                          Company
                        </th>
                        <th>
                          Work Title
                        </th>

                        <th>
                          Amount
                        </th>
                          <th>
                          At
                        </th>

                      </thead>
                      <tbody>

                       <tr>
                          <td>1</td>
                          <td>Muneem Group LTd</td>
                          <td>Developer</td>
                          <td>14000</td>
                          <td>26, january,2020</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      </div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
	$(document).ready(function() {
    $('#table').DataTable();
} );
</script>
@endpush
