;@extends('layouts.app')

@section('title','Dashboard')

@push('css')


@endpush

@section('content')
    <div class="GridLex-col-12_sm-12_xs-12" id="dashboard-info">

    <div class="admin-content-wrapper">

      <div class="admin-section-title">

        <h2>Hello , {{ Auth:: user()->name }}!</h2>
          <p>Your last loged-in: <span class="text-primary">11:42:21 am - March 15, 2016</span></p>
          <h3>Email: gateau@outlook.com</h3>
          <h4>Phone : +01969474545</h4>
          <h4>Address : 67/f , Green Road - Dhaka 1205 , Bangladesh</h4>

      </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
