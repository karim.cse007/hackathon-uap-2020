@extends('layouts.app')

@section('title','Research Paper profiles')

@push('css')


@endpush

@section('content')
    <div class="GridLex-col-12_sm-12_xs-12">

        <div class="admin-content-wrapper">

            <div class="">

                <h2>Education Details</h2>

            </div>

            <div class="job-item-grid-wrapper">

                <div class="GridLex-gap-30">

                    <div class="GridLex-grid-noGutter-equalHeight">

                        <div class="GridLex-col-12_sm-12_xs-12_xss-12">

                            <div class="job-item-grid">

                                <div class="content">
                                    <h4 class="heading">University Name</h4>
                                    <p class="location"><i class="fa fa-map-marker text-primary"></i>
                                        <strong class="text-primary">Degree </strong> - Bachalor</p>
                                    <p class="date text-muted font12 font-italic">Passing Year:</p>
                                    <p class="date text-muted font12 font-italic">23 hours ago</p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection

@push('scripts')

@endpush
