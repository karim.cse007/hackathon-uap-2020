  @extends('layouts.app')

  @section('title','Education')

  @push('css')


  @endpush

  @section('content')


  <div class="GridLex-col-9_sm-8_xs-12">
                
    <div class="admin-content-wrapper">

   <div class="admin-section-title">
                    
     <h2>Education</h2>
     <p>Your Education.</p>
                      
       </div>
                    
           <form class="post-form-wrapper">
                  
            <div class="row gap-20">
                      
                      <div class="col-sm-6 col-md-12">
                            
                              <div class="form-group">

                              <label>University Name</label>
                              <input type="text" class="form-control" value="">
                            </div>
                            
                          </div>
                         
                          <div class="form-group">
                          
                            <div class="col-sm-12">
                              <label>Degree</label>
                            </div>
                            
                            <div class="col-sm-6 col-md-12">
                              <select class="selectpicker show-tick form-control mb-15" data-live-search="false">
                                <option value="0">Select</option>
                                <option value="1">Diploma</option>
                                <option value="2" selected>Bachelor</option>
                                <option value="3">Master</option>
                                <option value="4">Doctoral </option>
                                <option value="5">Certificate</option>
                              </select>
                            </div>
                            

                              
                          </div>
                          
                          <div class="clear"></div>
                          
                          <div class="col-sm-6 col-md-12">
                          
                            <div class="form-group">
                              <label>Passing Year</label>
                              <input type="text" class="form-control" value="">
                            </div>
                            
                          </div>
                       
                           <div class="clear"></div>

                          <div class="col-sm-12 mt-10">
                            <a href="#" class="btn btn-primary">Save</a>
                            <a href="#" class="btn btn-primary btn-inverse">Cancel</a>
                             <a href="#" class="btn btn-primary">Add</a>
                          </div>

                        </div>
                        
                      </form>
                    
                  </div>

                </div>

    </div>

  @endsection

  @push('scripts')

  @endpush