@extends('layouts.app')

@section('title','Company Profile')

@push('css')


@endpush

@section('content')

<div class="GridLex-col-9_sm-8_xs-12">

    <div class="admin-content-wrapper">
    <div class="container-wrapper">

    <!-- start Main Wrapper -->
    <div class="main-wrapper">

      <div class="section sm">

        <div class="container">

          <div class="row">

            <div class="col-sm-8 col-md-9">

              <div class="blog-wrapper">

                <div class="blog-item blog-single">

                  <div class="blog-media">
                    <img src="{{ asset('frontend/images/blog/blog-01.jpg')}}" alt="" />
                  </div>

                  <div class="clear mb-50"></div>

                  <h4 class="uppercase">About Company</h4>
                  <div class="blog-author">
                    <div class="author-label">
                      <img src="{{ asset('frontend/images/man/01.jpg')}}" alt="author image" class="img-circle" />
                    </div>
                    <div class="author-details">
                      <ul class="social-share-sm pull-right pull-left-xs">
                        <li class="the-label"><a href="#">Facebook</a></li>
                        <li class="the-label"><a href="#">Twitter</a></li>
                        <li class="the-label"><a href="#">Google Plus</a></li>
                      </ul>
                      <div class="clear-xs"></div>
                      <h5 class="heading"><a href="#">{{auth()->user()->company->name}}</a></h5>
                      <p> {!! auth()->user()->company->description !!}</p>
                    </div>
                  </div>

                  <div class="clear mb-50"></div>
                    <div class="col-sm-12 col-md-12">

                        <form action="{{route('work.store')}}" method="POST" >
                            @csrf
                        <div class="form-group bootstrap3-wysihtml5-wrapper">
                            <label>Leave a Post</label>
                            <div class="col-sm-12 col-md-12">

                                <div class="form-group" id="title-show">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control" value="">
                                </div>
                                <div class="form-group" id="title-show">
                                    <label>Budget</label>
                                    <input type="number" name="price" class="form-control">
                                </div>

                            </div>
                            <textarea class="bootstrap3-wysihtml5 form-control" id="question_filed" name="description" placeholder="" style="height: 200px;">
                            </textarea>
                            <div class="form-group">
                                <select class="select2 form-control" name="fields[]" multiple required>
                                    @foreach($fields as $field)
                                        <option value="{{ $field->id }}">{{ $field->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 mt-10 hidden" id="submit-button">
                            <input type="submit" class="btn btn-primary" value="Post">
                        </div>
                        </form>
                    </div>

                  <div class="clear"></div>

                  <h3 class="uppercase">Posts</h3>

                    @foreach($works as $key=>$work)
                        <div id="comment-wrapper">
                            <ul class="comment-item">
                              <li>
                                <div class="comment-avatar">
                                  <img src="{{ asset('frontend/images/man/01.jpg')}}" alt="author image" />
                                </div>
                                <div class="comment-header">
                                  <h6 class="heading mt-0">{{$work->title}}</h6>
                                  <span class="comment-time">{{$work->created_at->diffForHumans()}}</span>
                                </div>
                                <div class="comment-content">
                                  <p>{!! $work->description !!}</p>
                                </div>
                                  <div class="comment-content">
                                      Budget: TK. {{$work->amount}}
                                  </div>
                                  <div class="comment-content">
                                      Skill:
                                      @foreach($work->workFields as $skill)
                                           {{$skill->name}},
                                      @endforeach
                                  </div>
                                  <div class="comment-content">
                                      Bid Info:
                                      @foreach($work->bids->where('work_id',$work->id) as $bid)
                                            <br></br>Name:{{$bid->user->name}}<br>
                                            Description: {{$bid->description}}
                                      @endforeach
                                  </div>
                              </li>
                            </ul>
                            <div class="clear"></div>
                        </div><!-- End Comment -->
                    @endforeach

                  <div class="clear"></div>

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>
    <!-- end Main Wrapper -->

  </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            //alert('working');
               $('#title-show').on('click',function () {
                  $('#submit-button').removeClass('hidden');
               });
        });
    </script>
@endpush
