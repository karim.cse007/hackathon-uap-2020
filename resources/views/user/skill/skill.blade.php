@extends('layouts.app')

@section('title','Skills')

@push('css')


@endpush

@section('content')


<div class="GridLex-col-9_sm-8_xs-12">
              
  <div class="admin-content-wrapper">

 <div class="admin-section-title">
                  
   <h2>Skills</h2>
   <p>Your Skills Tag.</p>
                    
     </div>
                  
        <form class="post-form-wrapper">
                
              <div class="row gap-20">
                    
                    <div class="col-sm-6 col-md-12">
                          
                          <div class="form-group">

                            <label>Skills Tag</label>
                            <input type="text" class="form-control" value="">
                          </div>
                          
                        </div>
                         <div class="clear"></div>

                        <div class="col-sm-12 mt-10">
                          <a href="#" class="btn btn-primary">Save</a>
                          <a href="#" class="btn btn-primary btn-inverse">Cancel</a>
                           
                        </div>

                      </div>
                      
                    </form>
                  
                </div>

              </div>

  </div>

@endsection

@push('scripts')

@endpush