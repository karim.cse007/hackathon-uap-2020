@extends('layouts.app')

@section('title',' Skill Profile')

@push('css')


@endpush

@section('content')
    <!-- start Forget Password Modal -->
    <div id="skill_info" class="modal fade login-box-wrapper" tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true">

        <div class="modal-body">
            <div class="row gap-20">
                <form method="POST" id="skill_form_submit">
                    @csrf
                    <p id="showprofileinfo"></p>
                    <div class="col-sm-12 col-md-12">

                            <div class="form-group">
                                <label>Skills</label>
                                <select class="form-control" id="skills" name="skills[]" multiple>
                                    <option>Select</option>
                                    @foreach($allSkill as $skill)

                                            <option value="{{$skill->id}}"
                                            @foreach($skills as $sk)
                                                {{$skill->id == $sk->id?'selected':''}}
                                                @endforeach >
                                                {{$skill->name}}</option>

                                    @endforeach
                                </select>
                            </div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="modal-footer text-center">
            <button type="button" data-dismiss="modal" class="btn btn-primary btn-inverse">Close</button>
        </div>

    </div>
    <!-- end Forget Password Modal -->
    <div class="GridLex-col-9_sm-8_xs-12">

        <div class="admin-content-wrapper">

            <div >

                <h2>Skill List</h2>

            </div>

            <div class="job-item-grid-wrapper">

                <div class="GridLex-gap-30">

                    <div class="GridLex-grid-noGutter-equalHeight">

                        <div class="GridLex-col-12_sm-6_xs-6_xss-12">

                            <div class="job-item-grid">
                                <a data-toggle="modal" href="#skill_info">
                                    <div class="content">
                                        <h4 class="heading">Skills</h4>
                                    </div>
                                </a>
                                <div class="content-bottom">
                                    <div class="sub-category">
                                        @foreach($skills as $skill)
                                            <a href="#">{{$skill->name}}</a>
                                        @endforeach
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $("#skill_form_submit").submit(function(){
                if( $('#skills').val() === ''){
                    var msg ="<span style='color:red;'>Error! <strong>Field Must not be empty!!!</strong></span>";
                    $("#showprofileinfo").html(msg);
                }else{
                    $.post("<?php echo url(route('user.skill.store')) ?>", $("#skill_form_submit").serialize(), function(data){
                        //alert("working");
                        $("#showprofileinfo").html(data);
                    });
                }
                return false;
            });
        });
    </script>
@endpush
