@extends('layouts.app')

@section('title','Achievement')

@push('css')


@endpush

@section('content')

<div class="GridLex-col-9_sm-8_xs-12">
              
  <div class="admin-content-wrapper">

 <div class="admin-section-title">
                  
   <h2>Achievements</h2>
   <p>Your Achievements.</p>
                    
     </div>
                  
                  <form class="post-form-wrapper">
                
                      <div class="row gap-20">
                    
                       
                        <div class="col-sm-6 col-md-4">
                        
                          <div class="form-group bootstrap-fileinput-style-01">
                            <label>Photo</label>
                            <input type="file" name="form-register-photo" id="form-register-photo">
                            <span class="font12 font-italic">** photo must not bigger than 250kb</span>
                          </div>
                          </div>
                        
                        <div class="clear"></div>
                        
                        <div class="col-sm-6 col-md-12">
                        
                          <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" value="">
                          </div>
                          
                        </div>
                        
                        <div class="col-sm-6 col-md-12">
                        
                          <div class="form-group">
                            <label>URL</label>
                            <input type="text" class="form-control" value="">
                          </div>
                          
                        </div>
                         <div class="clear"></div>

                        <div class="col-sm-12 mt-10">
                          <a href="#" class="btn btn-primary">Save</a>
                          <a href="#" class="btn btn-primary btn-inverse">Cancel</a>
                        </div>

                      </div>
                      
                    </form>
                  
                </div>

              </div>

  </div>

@endsection

@push('scripts')

@endpush