@extends('layouts.app')

@section('title','Achievement Profile ')

@push('css')


@endpush

@section('content')
    <div class="GridLex-col-9_sm-8_xs-12">

        <div class="admin-content-wrapper">

            <div>

                <h2> Achievement </h2>

            </div>

            <div class="job-item-grid-wrapper">

                <div class="GridLex-gap-30">

                    <div class="GridLex-grid-noGutter-equalHeight">

                        <div class="GridLex-col-12_sm-6_xs-6_xss-12">
                                <a href="#">

                                    <div class="image">

                                        <div class="vertical-middle">

                                            <img src="{{asset('frontend/images/01.jpg')}}" width="400px;" height="100px;" alt="image" />
                                        </div>
                                    </div>

                                    <div class="content">
                                        <h4 class="heading">NAME</h4>
                                        <p class="location"><i class="fa fa-map-marker text-primary"></i> <strong class="text-primary">URL</strong> - Paris, France</p>
                                        <p class="date text-muted font12 font-italic">23 hours ago</p>
                                    </div>

                                </a>



                            </div>

                        </div>




                        </div>








                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection

@push('scripts')

@endpush
