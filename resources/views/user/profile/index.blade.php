@extends('layouts.app')

@section('title','View Profile')

@push('css')


@endpush

@section('content')
<div class="GridLex-col-9_sm-8_xs-12">

  <div class="admin-content-wrapper">

 <div class="admin-section-title">

            <h2>View Profile</h2>
            <p>View Your Profile / Add New Profile.</p>

        </div>

        <div class="resume-list-wrapper">

            <div class="resume-list-item">

                <div class="row">

                    <div class="col-sm-12 col-md-10">

                        <div class="content">

                            <a href="#">

                                <div class="image">
                                    <img src="{{asset('frontend/images/man/01.jpg')}}" alt="images" class="img-circle" />
                                </div>

                                <h4>Suttira Ketkaew</h4>

                                <div class="row">
                                    <div class="col-sm-12 col-md-7">
                                        <strong class="mr-10">Senior Software Engineering</strong> <i class="fa fa-map-marker text-primary mr-5"></i> Marbomorn Company Ltd,.
                                    </div>
                                    <div class="col-sm-12 col-md-5 mt-10-sm">
                                        <i class="fa fa-calendar  text-primary mr-5"></i> Jan 2013 to Mar 2014
                                    </div>
                                </div>

                            </a>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-2">

                        <div class="resume-list-btn">
                            <a class="btn btn-primary btn-sm mb-5 mb-0-sm">Edit</a>
                            <a class="btn btn-primary btn-sm btn-inverse">Delete</a>
                        </div>

                    </div>


                </div>

            </div>

        </div>

        <div class="mt-30">

            <a href="#" class="btn btn-primary btn-lg">Add New Profile</a>

        </div>

    </div>

</div>

@endsection

@push('scripts')

@endpush
