@extends('layouts.app')

@section('title','Profile Details')

@push('css')


@endpush

@section('content')
        <div class="GridLex-col-9_sm-8_xs-12">
                            
                                <div class="admin-content-wrapper">

                                    <div class="admin-section-title">
                                    
                                        <h2>Profile Details</h2>
                                        <p>your profile details.</p>
                                        
                                    </div>

                                    <div class="job-item-grid-wrapper">
                    
                                        <div class="GridLex-gap-30">
                                        
                                            <div class="GridLex-grid-noGutter-equalHeight">
                                            
                                                <div class="GridLex-col-4_sm-6_xs-6_xss-12">
                                                
                                                    <div class="job-item-grid">
                                                    
                                                        <div class="fav-save">
                                                        
                                                            <a href="#"><i class="fa fa-heart text-danger"></i></a>
                                                        
                                                        </div>
                                                        <div class="labeling">
                                                            <span class="label label-success">Freelance</span>
                                                        </div>
                                                        
                                                        <a href="#">
                                                        
                                                            <div class="image">
                                                            
                                                                <div class="vertical-middle">
                                                                
                                                                    <img src="{{asset('frontend/images/brands/01.png')}}" alt="image" />
                                                                
                                                                </div>
                                                                
                                                            </div>
                                                        
                                                            <div class="content">
                                                                <h4 class="heading">Packaging Engineer</h4>
                                                                <p class="location"><i class="fa fa-map-marker text-primary"></i> <strong class="text-primary">Expedia</strong> - Paris, France</p>
                                                                <p class="date text-muted font12 font-italic">23 hours ago</p>
                                                            </div>
                                                            
                                                        </a>
                                                        
                                                        <div class="content-bottom">
                                                            <div class="sub-category">
                                                                <a href="#">Engineer</a>
                                                                <a href="#">Packaging</a>
                                                                <a href="#">Package</a>
                                                                <a href="#">Manufacturing</a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                                
                                            
                                         <div class="GridLex-col-4_sm-6_xs-6_xss-12">
                                                
                                                    <div class="job-item-grid">
                                                    
                                                        <div class="fav-save">
                                                        
                                                            <a href="#"><i class="fa fa-heart text-danger"></i></a>
                                                        
                                                        </div>
                                                        <div class="labeling">
                                                            <span class="label label-danger">Part-time</span>
                                                        </div>
                                                        
                                                        <a href="#">
                                                        
                                                            <div class="image">
                                                            
                                                                <div class="vertical-middle">
                                                                
                                                                    <img src="{{asset('frontend/images/brands/01.png')}}" alt="image" />
                                                                
                                                                </div>
                                                                
                                                            </div>
                                                        
                                                            <div class="content">
                                                                <h4 class="heading">Mechanical Engineer</h4>
                                                                <p class="location"><i class="fa fa-map-marker text-primary"></i> <strong class="text-primary">Freelancer</strong> - Paris, France</p>
                                                                <p class="date text-muted font12 font-italic">23 hours ago</p>
                                                            </div>
                                                            
                                                        </a>
                                                        
                                                        <div class="content-bottom">
                                                            <div class="sub-category">
                                                                <a href="#">Engineer</a>
                                                                <a href="#">Packaging</a>
                                                                <a href="#">Package</a>
                                                                <a href="#">Manufacturing</a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>       
                                    
                                              
                                           
                                                
                                               
                                               
                                               

                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>

                            </div>

@endsection

@push('scripts')

@endpush