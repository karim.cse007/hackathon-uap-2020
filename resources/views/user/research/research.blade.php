@extends('layouts.app')

@section('title','Research Paper')

@push('css')


@endpush

@section('content')


<div class="GridLex-col-9_sm-8_xs-12">
              
  <div class="admin-content-wrapper">

 <div class="admin-section-title">
                  
   <h2>Research Paper</h2>
   <p>Your Research Paper.</p>
                    
     </div>
                  
                  <form class="post-form-wrapper">
                
                      <div class="row gap-20">
                    
                    <div class="col-sm-6 col-md-12">
                          
                          <div class="form-group">

                            <label>Paper Title</label>
                            <input type="text" class="form-control" value="">
                          </div>
                          
                        </div>


                    <div class="col-sm-6 col-md-12">
                          
                          <div class="form-group">

                            <label>Conference Name</label>
                            <input type="text" class="form-control" value="">
                          </div>
                          
                        </div>
                       
                       
                        
                       
                        
                        
                        
                        <div class="clear"></div>
                        
                        <div class="col-sm-6 col-md-12">
                        
                          <div class="form-group">
                            <label>Paper Link</label>
                            <input type="text" class="form-control" value="">
                          </div>
                          
                        </div>
                     
                        
                   
                     

                        
                        
                       
                        
                        <div class="clear"></div>

                        <div class="col-sm-12 mt-10">
                          <a href="#" class="btn btn-primary">Save</a>
                          <a href="#" class="btn btn-primary btn-inverse">Cancel</a>
                           
                        </div>

                      </div>
                      
                    </form>
                  
                </div>

              </div>

  </div>

@endsection

@push('scripts')

@endpush