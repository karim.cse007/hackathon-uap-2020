@extends('layouts.app')

@section('title','Research Paper profiles')

@push('css')


@endpush

@section('content')
    <div class="GridLex-col-9_sm-8_xs-12">

        <div class="admin-content-wrapper">

            <div >

                <h2>Research Paper List</h2>

            </div>

            <div class="job-item-grid-wrapper">

                <div class="GridLex-gap-30">

                    <div class="GridLex-grid-noGutter-equalHeight">

                        <div class="GridLex-col-12_sm-6_xs-6_xss-12">

                            <div class="job-item-grid">

                                <div class="content">
                                        <h4 class="heading">Paper Title</h4>
                                        <p class="location"><i class="fa fa-map-marker text-primary"></i> <strong class="text-primary">Conference Name </strong> - ITEEE Lnags</p>
                                        <p class="date text-muted font12 font-italic">Link:</p>
                                        <p class="date text-muted font12 font-italic">23 hours ago</p>
                                    </div>





                            </div>

                        </div>










                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection

@push('scripts')

@endpush
