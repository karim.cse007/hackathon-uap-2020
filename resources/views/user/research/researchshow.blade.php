@extends('layouts.app')

@section('title','Research Paper Show')

@push('css')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">

@endpush

@section('content')

<div class="GridLex-col-9_sm-8_xs-12">
                
    <div class="admin-content-wrapper">

   <div class="admin-section-title">
          <div class="row">
            <div class="col-md-12">
               <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Research Paper</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="table" class="table" style="width:100%">
                      <thead class=" text-primary">
                        <th>
                          ID
                        </th>
                        <th>
                          Company
                        </th>
                        <th>
                          Work Title
                        </th>
                       
                        <th>
                          Amount
                        </th>
                          <th>
                          Created At
                        </th>
                        <th>
                          Updated At
                        </th>
                        <th>Action</th>
                      </thead>
                      <tbody>
                      
                       <tr>
                                          <td>1</td>  
                                          <td>Muneem Group LTd</td> 
                                          <td>Developer</td>  
                                          <td>14000</td>   
                                          <td>22.05.00</td> 
                                           <td>22.05.00</td>  
                                               <td>
                                                <a href="" class="btn btn-info btn-sm"><i class="material-icons">Edit</i></a>

                                                <form id="delete-form" action="" style="display: none;" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure? You want to delete this?')){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form').submit();
                                                }else {
                                                    event.preventDefault();
                                                        }"><i class="material-icons">delete</i></button>
                                            </td> 
                                        </tr>
                       


                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      </div>
@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
	$(document).ready(function() {
    $('#table').DataTable();
} );
</script>
@endpush