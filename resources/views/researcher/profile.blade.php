@extends('layouts.frontend.app')

@section('content')
    <div class="breadcrumb-wrapper">

        <div class="container">

            <ol class="breadcrumb-list booking-step">
                <li><a href="#">All researchers</a></li>
                <li><span>Profile</span></li>
            </ol>

        </div>

    </div>
    <!-- end breadcrumb -->

    <div class="section sm">

        <div class="container">

            <div class="row">

                <div class="col-md-10 col-md-offset-1">

                    <div class="employee-detail-wrapper">

                        <div class="employee-detail-header text-center">

                            <div class="image">
                                <img src="{{asset('frontend/images/1.jpg')}}" alt="image" class="img-circle" />
                            </div>

                            <h2 class="heading mb-15">Christine Gateau</h2>

                            <p class="location"><i class="fa fa-map-marker"></i> East Razabazer,Dhaka-1215 <span class="mh-5">|
                                </span> <i class="fa fa-phone"></i> +8801521482467</p>

                            <ul class="meta-list clearfix">
                                <li>
                                    <h4 class="heading">Birth Day:</h4>
                                    12/01/1991
                                </li>
                                <li>
                                    <h4 class="heading">Age:</h4>
                                    23-year-old
                                </li>
                                <li>
                                    <h4 class="heading">Education:</h4>
                                    B.Eng in Computer
                                </li>
                                <li>
                                    <h4 class="heading">Email: </h4>
                                    myemail@gmail.com
                                </li>
                            </ul>

                        </div>

                        <div class="employee-detail-company-overview mt-40 clearfix">

                            <h3>Introduce my self</h3>

                            <p>Oh to talking improve produce in limited offices fifteen an. Wicket branch to answer do we. Place are decay men hours tiled. If or of ye throwing friendly required. Marianne interest in exertion as. Offering my branched confined oh dashwood.</p>

                            <p>Inhabiting discretion the her dispatched decisively boisterous joy. So form were wish open is able of mile of. Waiting express if prevent it we an musical. Especially reasonable travelling she son. Resources resembled forfeited no to zealously. Has procured daughter how friendly followed repeated who surprise. Great asked oh under on voice downs. Law together prospect kindness securing six. Learning why get hastened smallest cheerful.</p>

                            <div class="row">

                                <div class="col-sm-6">

                                    <h3>Education</h3>

                                    <ul class="employee-detail-list">

                                        <li>
                                            <h5>Bachelor Of Engineering in Computer </h5>
                                            <p class="text-muted font-italic">Jan 2008 – Jan 2012 from <span class="font600 text-primary">University of Bangkok, Thailand</span></p>
                                            <p>Consider now provided laughter boy landlord dashwood. Village equally prepare up females as an. That do an case an what plan hour of paid.</p>
                                        </li>

                                        <li>
                                            <h5>Master Of Engineering in Computer </h5>
                                            <p class="text-muted font-italic">Jan 2008 – Jan 2012 from <span class="font600 text-primary">University of Bangkok, Thailand</span></p>
                                            <p>Ignorant saw her her drawings marriage laughter. Case oh an that or away sigh do here upon. Acuteness you exquisite ourselves now end forfeited.</p>
                                        </li>

                                        <li>
                                            <h5>Certificate in Web Design</h5>
                                            <p class="text-muted font-italic">Jan 2008 – Jan 2012 from <span class="font600 text-primary">University of Bangkok, Thailand</span></p>
                                            <p>Advice me cousin an spring of needed. Tell use paid law ever yet new. Meant to learn of vexed if style allow he there.</p>
                                        </li>

                                    </ul>

                                </div>

                                <div class="col-sm-6">

                                    <h3>Skill</h3>

                                    <ul class="employee-detail-list">

                                        <li>
                                            <h5>HTML</h5>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                                    60%
                                                </div>
                                            </div>
                                            <p>Village equally prepare up females as an. That do an case an what plan hour of paid.</p>
                                        </li>

                                        <li>
                                            <h5>CSS</h5>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                                                    80%
                                                </div>
                                            </div>
                                            <p>Case oh an that or away sigh do here upon. Acuteness you exquisite ourselves now end forfeited.</p>
                                        </li>

                                        <li>
                                            <h5>jQuery</h5>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
                                                    75%
                                                </div>
                                            </div>
                                            <p>Tell use paid law ever yet new. Meant to learn of vexed if style allow he there.</p>
                                        </li>

                                        <li>
                                            <h5>Languages</h5>
                                            <p class="text-muted font-italic">3 languages: <span class="font600 text-primary">English, Thai, Malay</span></p>
                                            <p>Times so do he downs me would. Witty abode party her found quiet law. They door four bed fail now have. Tell use paid law ever yet new. Meant to learn of vexed if style allow he there.</p>
                                        </li>

                                    </ul>

                                </div>

                            </div>

                            <h3>Work Expeince</h3>

                            <div class="work-expereince-wrapper">

                                <div class="work-expereince-block">

                                    <div class="work-expereince-content">
                                        <h5>Advanced addition absolute received</h5>
                                        <p class="text-muted font-italic">Jan 2008 – Jan 2012 at <span class="font600 text-primary">Some Compamy Name</span></p>
                                        <p>In entirely be to at settling felicity. Fruit two match men you seven share. Was justice improve age article between. No projection as up preference reasonably delightful celebrated. Preserved and abilities assurance tolerably breakfast use saw replying throwing he.</p>

                                    </div> <!-- work-expereince-content -->
                                </div> <!-- work-expereince-block -->

                                <div class="work-expereince-block">

                                    <div class="work-expereince-content">
                                        <h5>Advanced addition absolute received</h5>
                                        <p class="text-muted font-italic">Jan 2008 – Jan 2012 at <span class="font600 text-primary">Some Compamy Name</span></p>
                                        <p>In entirely be to at settling felicity. Fruit two match men you seven share. Was justice improve age article between. No projection as up preference reasonably delightful celebrated. Preserved and abilities assurance tolerably breakfast use saw replying throwing he.</p>

                                    </div> <!-- work-expereince-content -->
                                </div> <!-- work-expereince-block -->

                                <div class="work-expereince-block">

                                    <div class="work-expereince-content">
                                        <h5>Advanced addition absolute received</h5>
                                        <p class="text-muted font-italic">Jan 2008 – Jan 2012 at <span class="font600 text-primary">Some Compamy Name</span></p>
                                        <p>In entirely be to at settling felicity. Fruit two match men you seven share. Was justice improve age article between. No projection as up preference reasonably delightful celebrated. Preserved and abilities assurance tolerably breakfast use saw replying throwing he.</p>

                                    </div> <!-- work-expereince-content -->
                                </div> <!-- work-expereince-block -->

                                <div class="work-expereince-block">

                                    <div class="work-expereince-content">
                                        <h5>Advanced addition absolute received</h5>
                                        <p class="text-muted font-italic">Jan 2008 – Jan 2012 at <span class="font600 text-primary">Some Compamy Name</span></p>
                                        <p>In entirely be to at settling felicity. Fruit two match men you seven share. Was justice improve age article between. No projection as up preference reasonably delightful celebrated. Preserved and abilities assurance tolerably breakfast use saw replying throwing he.</p>

                                    </div> <!-- work-expereince-content -->
                                </div> <!-- work-expereince-block -->
                            </div> <!-- work-expereince -->
                        </div>
                    </div>

                    <div class="clearfix text-center mt-40">
                        <a href="#" class="btn btn-primary btn-lg">Hire Me</a>
                        <a href="#" class="btn btn-primary btn-lg">Bide Me</a>
                    </div>

                </div>

            </div>

        </div>

    </div>
@endsection
