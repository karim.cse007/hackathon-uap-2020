@extends('layouts.chat.app')

@section('content')
    <div id="app">
        <caller :user="{{auth()->user()}}" :receiver="{{ $receiver }}"></caller>
    </div>
@endsection
@push('js')
    <script>
        window.user={
            id:"{{auth()->id()}}",
            name:"{{auth()->user()->name}}",
        };
        window.csrfToken="{{csrf_token()}}";
    </script>
@endpush
