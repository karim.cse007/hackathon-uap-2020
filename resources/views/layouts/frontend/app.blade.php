<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fav and Touch Icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('frontend/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('frontend/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('frontend/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('frontend/images/ico/apple-touch-icon-57-precomposed.png')}}">
    <link rel="shortcut icon" href="{{asset('frontend/images/ico/favicon.png')}}">

    <!-- CSS Plugins -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/bootstrap/css/bootstrap.min.css')}}" media="screen">
    <link href="{{asset('frontend/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/component.css')}}" rel="stylesheet">

    <!-- CSS Font Icons -->
    <link rel="stylesheet" href="{{asset('frontend/icons/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/icons/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/icons/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/icons/ionicons/css/ionicons.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/icons/rivolicons/style.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/icons/flaticon-line-icon-set/flaticon-line-icon-set.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/icons/flaticon-streamline-outline/flaticon-streamline-outline.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/icons/flaticon-thick-icons/flaticon-thick.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/icons/flaticon-ventures/flaticon-ventures.css')}}">

    <!-- CSS Custom -->
    <link href="{{asset('frontend/css/style.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  @stack('push')
</head>
<body class="home">

    <div id="introLoader" class="introLoading"></div>

    <!-- start Container Wrapper -->
    <div class="container-wrapper">
        @include('layouts.frontend.inc.header')
        <!-- start Main Wrapper -->
            <div class="main-wrapper">
                @yield('content')
                @include('layouts.frontend.inc.footer')
            </div>
    </div>


    <!-- start Back To Top -->
    <div id="back-to-top">
        <a href="#"><i class="ion-ios-arrow-up"></i></a>
    </div>
    <!-- end Back To Top -->


    <!-- JS -->
    <script type="text/javascript" src="{{asset('frontend/js/jquery-1.11.3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery-migrate-1.2.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/bootstrap-modalmanager.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/bootstrap-modal.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/smoothscroll.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery.waypoints.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery.slicknav.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery.placeholder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/bootstrap-tokenfield.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/typeahead.bundle.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/bootstrap3-wysihtml5.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery-filestyle.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/bootstrap-select.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/ion.rangeSlider.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/handlebars.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery.countimator.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery.countimator.wheel.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/slick.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/easy-ticker.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery.introLoader.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/jquery.responsivegrid.js')}}"></script>
    <script type="text/javascript" src="{{asset('frontend/js/customs.js')}}"></script>

  @stack('scripts')
</body>

</html>
