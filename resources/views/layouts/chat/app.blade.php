<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('chat/dist/css/soho.min.css') }}" rel="stylesheet">
 </head>
<body>
    <!-- page loading -->
    <div class="page-loading"></div>
    <!-- ./ page loading -->
    <div id="app">

        @include('layouts.chat.inc.model')
        <div class="layout">
            @include('layouts.chat.inc.sidebar')
            <div class="content">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- JQuery -->
    <script src="{{asset('chat/vendor/jquery-3.4.1.min.js')}}"></script>

    <!-- Popper.js -->
    <script src="{{asset('chat/vendor/popper.min.js')}}"></script>

    <!-- Bootstrap -->
    <script src="{{asset('chat/vendor/bootstrap/bootstrap.min.js')}}"></script>

    <!-- Nicescroll -->
    <script src="{{asset('chat/vendor/jquery.nicescroll.min.js')}}"></script>

    <!-- Soho -->
    <script src="{{asset('chat/dist/js/soho.min.js')}}"></script>
    <script src="{{asset('chat/dist/js/examples.js')}}"></script>
@stack('js')
</body>
</html>
