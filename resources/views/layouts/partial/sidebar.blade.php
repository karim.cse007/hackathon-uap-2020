		<div class="admin-sidebar">

		<div class="admin-user-item col-md-12">
				<div class="image">
					<img src="{{asset('frontend/images/man/01.jpg')}}" alt="image" class="img-circle" />
					</div>

                                <h4>Christine</h4>
                                <p class="user-role">Professional</p>

                            </div>
                        <ul class="admin-user-menu clearfix col-md-12">
                        @if (auth()->user()->role->id==3)
                                <li>
                                    <a href="{{ route('user.dashboard') }}"><i class="fa fa-pencil"></i> Home</a>
                                </li>
                            <li>
                                <a href="{{ route('user.edit') }}"><i class="fa fa-pencil"></i> Edit Profile</a>
                            </li>
                            <li>
                                <a href="{{ route('user.educationdetail') }}"><i class="fa fa-graduation-cap"></i>Education</a>
                            </li>
                            <li>
                                <a href="{{route('user.researchdetail')}}"><i class="fa fa-briefcase"></i>Research Paper</a>
                            </li>
                            <li>
                                <a href="{{route('user.achievementdetail')}}"><i class="fa fa-bookmark"></i> Achievements</a>
                            </li>
                            <li>
                                <a href="{{ route('user.skilldetail') }}"><i class="fa fa-cogs"></i>  Skills </a>
                            </li>
                            <li>
                                <a href="{{ route('user.work') }}"><i class="fa fa-archive"></i> Work History</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-bell"></i> Account</a>
                            </li>
                        @elseif (auth()->user()->role->id==2)
                                <li>
                                    <a href="{{route('user.company')}}"><i class="fa fa-archive"></i> Home</a>
                                </li>
                                <li>
                                    <a href="{{route('company.history')}}"><i class="fa fa-archive"></i> Work History</a>
                                </li>
                        @endif
                            <li>
                                <a href="{{route('message')}}" target="_blank"><i class="fa fa-comment"></i> Chat</a>
                            </li>

                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Logout</a>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </ul>

                    </div>
