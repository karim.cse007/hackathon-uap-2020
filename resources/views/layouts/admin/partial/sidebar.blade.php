 <div class="sidebar" data-color="purple" data-background-color="white" src="{{ asset('backend/img/sidebar-3.jpg') }}">

      <div class="logo">
        <a href="{{route('admin.dashboard')}}" class="simple-text logo-normal">
           Admin
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="{{Request::is('admin/dashboard*') ? 'active': ''}} ">
            <a class="nav-link" href="{{route('admin.dashboard')}}">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="{{Request::is('admin/university*') ? 'active': ''}}">
            <a class="nav-link" href="{{route('university.index')}}">
              <i class="material-icons">school</i>
              <p>University / School</p>
            </a>
          </li>
          <li class="{{Request::is('admin/degree*') ? 'active': ''}}">
            <a class="nav-link" href="{{route('degree.index')}}">
              <i class="material-icons">group_work</i>
              <p>Degree</p>
            </a>
          </li>
           <li class="{{Request::is('admin/tag*') ? 'active': ''}}">
            <a class="nav-link" href="{{route('tag.index')}}">
              <i class="material-icons">work</i>
              <p>Work field</p>
            </a>
          </li>
        <li class="{{Request::is('admin/account*') ? 'active': ''}}">
            <a class="nav-link" href="{{route('account.index')}}">
              <i class="material-icons">schedule</i>
              <p>Account</p>
            </a>
          </li>

        </ul>
      </div>
    </div>
