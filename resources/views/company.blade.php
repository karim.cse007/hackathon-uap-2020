@extends('layouts.frontend.app')

@section('title','Home')

@push('css')

@endpush('css')

@section('content')
    <div class="GridLex-col-9_sm-8_xs-12">

        <div class="admin-content-wrapper">
            <div class="container-wrapper">

                <!-- start Main Wrapper -->
                <div class="main-wrapper">

                    <div class="section sm">

                        <div class="container">

                            <div class="row">

                                <div class="col-sm-12 col-md-12">

                                    <div class="blog-wrapper">

                                        <div class="blog-item blog-single">

                                            <div class="blog-media">
                                                <img src="{{ asset('frontend/images/blog/blog-01.jpg')}}" width="100%" alt="" />
                                            </div>

                                            <div class="clear mb-50"></div>

                                            <h4 class="uppercase">About Company</h4>
                                            <div class="blog-author">
                                                <div class="author-label">
                                                    <img src="{{ asset('frontend/images/2.jpg')}}" alt="author image" class="img-circle" />
                                                </div>
                                                <div class="author-details">
                                                    <ul class="social-share-sm pull-right pull-left-xs">
                                                        <li class="the-label"><a href="#">Facebook</a></li>
                                                        <li class="the-label"><a href="#">Twitter</a></li>
                                                        <li class="the-label"><a href="#">Google Plus</a></li>
                                                    </ul>
                                                    <div class="clear-xs"></div>
                                                    <h5 class="heading"><a href="#">{{$company->name}}</a></h5>
                                                    <p> {!! $company->description !!}</p>
                                                </div>
                                            </div>

                                            <div class="clear mb-50"></div>

                                            <div class="clear"></div>

                                            <h3 class="uppercase">Posts</h3>

                                            @foreach($works as $key=>$work)
                                                <div id="comment-wrapper">
                                                    <ul class="comment-item">
                                                        <li>
                                                            <div class="comment-avatar">
                                                                <img src="{{ asset('frontend/images/man/01.jpg')}}" alt="author image" />
                                                            </div>
                                                            <div class="comment-header">
                                                                <h6 class="heading mt-0">{{$work->title}}</h6>
                                                                <span class="comment-time">{{$work->created_at->diffForHumans()}}</span>
                                                            </div>
                                                            <div class="comment-content">
                                                                <p>{!! $work->description !!}</p>
                                                            </div>
                                                            <div class="comment-content">
                                                                Budget: Tk. {{$work->amount}}
                                                            </div>
                                                            <div class="comment-content">
                                                                @foreach($work->workFields as $field)
                                                                    Skills: {{$field->name}},
                                                                @endforeach
                                                            </div>
                                                            <div class="comment-content">
                                                                <button id="bid" onclick="bid({{$work->id}})" class="btn btn-primary">Bid Now</button>
                                                            </div>
                                                            <p id="bid-info-{{$work->id}}"></p>
                                                            <div class="comment-content hidden" id="bid_section-{{$work->id}}">
                                                                <form action="javascript:void(0);" method="POST" id="bid-form-submit-{{$work->id}}">
                                                                    @csrf
                                                                    <textarea class="form-control" name="bid"></textarea>
                                                                    <input type="hidden" name="work" value="{{$work->id}}">
                                                                    <button type="submit" id="bid-submit-{{$work->id}}" onclick="makebid({{$work->id}})" class="btn btn-primary right"> Submit</button>
                                                                </form>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div class="clear"></div>
                                                </div><!-- End Comment -->

                                            @endforeach

                                            <div class="clear"></div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>
                <!-- end Main Wrapper -->

            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function bid(id) {
            $('#bid_section-'+id).removeClass('hidden');
        }
        function makebid(id) {
            $.post("<?php echo url(route('user.bid')) ?>", $("#bid-form-submit-"+id).serialize(), function(data){
                $('#bid-info-'+id).html(data);
                $('#bid_section-'+id).addClass('hidden');
            });
        }
    </script>
@endpush

