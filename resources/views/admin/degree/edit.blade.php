@extends('layouts.admin.app')

@section('title','Edit')

@push('css')

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                   <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title">Edit Degree</h4>
                        </div>
                        <div class="card-content">
                            <form method="POST" action="{{ route('item.update',$item->id) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                              
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Degree Name</label>
                                            <input type="text" class="form-control" value="" name="name">
                                        </div>
                                    </div>
                                </div>
                          
                             
                                <a href="{{ route('degree.index') }}" class="btn btn-danger">Back</a>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush