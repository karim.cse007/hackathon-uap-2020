@extends('layouts.admin.app')

@section('title','Account')

@push('css')
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('account.create') }}" class="btn btn-primary">Add New</a>
                  <div class="card">
                        <div class="card-header card-header-primary">
                         <h4 class="card-title ">All Accounts</h4>
                        </div>

                        <div class="card-content table-responsive">
                            <table id="table" class="table"  cellspacing="0" width="100%">
                                <thead class="text-primary">
                                <th>SL</th>
                                <th>Name</th>
                                <th>Amount</th>
                                <th>Work History</th>
                               <th>At</th>
                                </thead>
                                <tbody>
                                    <tr>
                                      <td>1</td>
                                      <td>Kaisar Jamil</td>
                                      <td>14000</td>
                                      <td>Kaisar Jamil</td>
                                      <td>26,January,2020</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
  $(document).ready(function() {
    $('#table').DataTable();
} );
</script>
@endpush
