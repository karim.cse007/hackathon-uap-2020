@extends('layouts.chat.app')

@section('content')
    <Chat :user="{{auth()->user()}}"></Chat>
@endsection
@push('js')
<script>
    window.user={
        id:"{{auth()->id()}}",
        name:"{{auth()->user()->name}}",
    };
    window.csrfToken="{{csrf_token()}}";
</script>
@endpush
