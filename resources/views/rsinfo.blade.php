@extends('layouts.frontend.app')

@section('title','Home')

@push('css')

@endpush('css')

@section('content')
    <div class="second-search-result-wrapper">

        <div class="container">
        </div>

    </div>

    <!-- start hero-header -->
    <div class="breadcrumb-wrapper">

        <div class="container">

            <ol class="breadcrumb-list booking-step">
                <li><a href="#">Researcher</a></li>
                <li><span>all researchers</span></li>
            </ol>

        </div>

    </div>
    <!-- end hero-header -->

    <div class="section sm">

        <div class="container">

            <div class="">

                <div class="container">

                    <form action="{{route('search.researcher')}}" method="GET" id="search_researcher">
                        @csrf
                        <div class="second-search-result-inner">
                            <span class="labeling">Search <br> Researcher</span>
                            <div class="row">
                                <div class="col-sm-4 col-md-5">
                                    <div class="form-group form-lg">
                                        <input type="text" name="search" class="form-control" placeholder="Name: Ex: Researcher" />
                                    </div>
                                </div>

                                <div class="col-sm-4 col-md-2">
                                    <button type="submit" class="btn btn-block">Search</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

            <div class="employee-grid-wrapper" id="show_researcher">

                <div class="GridLex-gap-15-wrappper">

                    <div class="GridLex-grid-noGutter-equalHeight">
                        @foreach($researchers as $research)
                            <div class="GridLex-col-3_sm-4_xs-6_xss-12">
                                <div class="employee-grid-item">

                                    <div class="action">

                                        <div class="row gap-10">

                                            <div class="col-xs-6 col-sm-6">
                                                <div class="text-left">
                                                    <button class="btn"><i class="icon-heart"></i></button>
                                                </div>
                                            </div>

                                            <div class="col-xs-6 col-sm-6">
                                                <div class="text-right">
                                                    <a class="btn text-right" href=""><i class="icon-action-redo"></i></a>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <a href="{{ route('profile')}}" class="clearfix">

                                        <div class="image clearfix">

                                            <img src="{{asset('frontend/images/1.jpg')}}" alt="Image" class="img-circle" >

                                        </div>

                                        <div class="content">

                                            <h4>{{$research->name}}</h4>
                                            <p class="location"> {{$research->email}}</p>

                                            <h6 class="text-primary">Skills:</h6>
                                            <ul class="employee-skill clearfix">
                                                @foreach($research->skills as $skill)
                                                    <li><span>{{$skill->name}}</span></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>

            <div class="pager-wrapper">

            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
