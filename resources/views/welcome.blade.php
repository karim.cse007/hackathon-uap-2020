@extends('layouts.frontend.app')

@section('title','Home')

@push('css')

@endpush('css')

@section('content')
    <!-- start hero-header -->
    <div class="hero" style="background-image:url('{{asset('frontend/images/hero-header/01.jpg')}}');">
        <div class="container">

            <h1>your future starts here now</h1>
            <p>Finding your next job or career more 1000+ availabilities</p>

            <div class="main-search-form-wrapper">

                <form>

                    <div class="form-holder">
                        <div class="row gap-0">

                            <div class="col-xss-6 col-xs-6 col-sm-6">
                                <input class="form-control" placeholder="Looking for job" />
                            </div>

                            <div class="col-xss-6 col-xs-6 col-sm-6">
                                <input class="form-control" placeholder="Place to work" />
                            </div>
                        </div>
                    </div>
                    <div class="btn-holder">
                        <button class="btn"><i class="ion-android-search"></i></button>
                    </div>

                </form>

            </div>


        </div>

    </div>
    <!-- end hero-header -->


    <div class="bg-light pt-80 pb-50">

        <div class="container">

            <div class="row">

                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">

                    <div class="section-title">

                        <h2>Find jobs by category</h2>

                    </div>

                </div>

            </div>

            <div class="category-group-wrapper">

                <div class="GridLex-gap-30 no-mb">

                    <div class="GridLex-grid-noGutter-equalHeight">

                        <div class="GridLex-col-6_sm-6_xs-12_xss-12">

                            <div class="category-group">

                                <div class="icon">
                                    <i class="flaticon-streamline-outline-computer"></i>
                                </div>

                                <h5>Researcher</h5>
                                <p>Or kind rest bred with am shed then.</p>
                                <p class="sub-category">
                                    <a href="#">Andriod</a>
                                    <a href="#">Web</a>
                                    <a href="#">Networking</a>
                                    <a href="#">Programming</a>
                                    <a href="#">More</a>
                                </p>

                            </div>

                        </div>

                        <div class="GridLex-col-6_sm-6_xs-12_xss-12">

                            <div class="category-group">

                                <div class="icon">
                                    <i class="flaticon-streamline-outline-crop"></i>
                                </div>

                                <h5>Industry</h5>
                                <p>In raptures building an bringing be.</p>
                                <p class="sub-category">
                                    <a href="#">Process Engineer</a>
                                    <a href="#">Quality Engineer</a>
                                    <a href="#">Design Engineer</a>
                                    <a href="#">Civil Engineer</a>
                                    <a href="#">More</a>
                                </p>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>

    </div>

    <div class="pt-80 pb-80">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="section-title">

                        <h2 class="text-left text-center-sm">Latest Jobs</h2>
                    </div>
                    <div class="recent-job-wrapper">
                        @foreach($works as $work)
                            <a href="{{route('company.details',$work->company_id)}}" class="recent-job-item highlight clearfix">
                            <div class="GridLex-grid-middle">
                                <div class="GridLex-col-6_xs-12">
                                    <div class="job-position">
                                        <div class="content">
                                            <h4>{{$work->title}}</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="GridLex-col-4_xs-8_xss-12 mt-10-xss">
                                    <div class="job-location">
                                        @foreach($work->workFields as $field)
                                            {{$field->name}},
                                        @endforeach
                                    </div>
                                </div>
                                <div class="GridLex-col-2_xs-4_xss-12">
                                    <div class="job-label label label-success">
                                        Show
                                    </div>
                                    <span class="font12 block spacing1 font400 text-center">{{$work->created_at->diffForHumans()}}</span>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="container pt-80 pb-80">

        <div class="row">

            <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">

                <div class="section-title mb-50">

                    <h2>you can be the next</h2>
                    <p>But extremity sex now education concluded earnestly her continual.</p>

                </div>

            </div>

        </div>

        <div class="text-center">

            <a href="#" class="btn btn-lg btn-primary mt-5-xss">Account</a>
        </div>

    </div>
@endsection

@push('scripts')

@endpush

