<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    public function educations(){
        return $this->hasMany(Education::class);
    }
}
