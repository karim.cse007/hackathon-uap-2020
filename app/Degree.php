<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Degree extends Model
{
    public function educations(){
        return $this->hasMany(Education::class);
    }
}
