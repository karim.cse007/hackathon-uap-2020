<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\WorkField;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TagController extends Controller
{
    public function index()
    {
        $tags= WorkField::all();
        return view('admin.tag.index',compact('tags'));
    }
    public function create(){
        return view('admin.tag.create');
    }
    public function store(Request $request){

        $val = Validator::make($request->all(),[
            'name'=> 'string|required|unique:work_fields,name|max:191'
        ]);
        if ($val->fails()){
            Toastr::error('Invalid request','Error',["positionClass" => "toast-top-center"]);
            return redirect()->back();
        }
        $uv = new WorkField();
        $uv->name=$request->name;
        $uv->save();
        Toastr::success('Successfully added','Success',["positionClass" => "toast-top-center"]);
        return redirect()->back();
    }
    public function destroy($id){
        $uv = WorkField::findOrFail($id);
        $uv->delete();
        Toastr::success('Successfully delete','Success');
        return redirect()->back();
    }
}
