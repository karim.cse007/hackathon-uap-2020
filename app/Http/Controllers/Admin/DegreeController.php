<?php

namespace App\Http\Controllers\Admin;

use App\Degree;
use App\Http\Controllers\Controller;
use App\University;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DegreeController extends Controller
{
    public function index()
    {
        $degries = Degree::all();
        return view('admin.degree.index',compact('degries'));
    }
    public function create(){
        return view('admin.degree.create');
    }
    public function store(Request $request){
        $val = Validator::make($request->all(),[
            'name'=> 'string|required|unique:degrees,name|max:191'
        ]);
        if ($val->fails()){
            Toastr::error('Invalid request','Error',["positionClass" => "toast-top-center"]);
            return redirect()->back();
        }
        $uv = new Degree();
        $uv->name=$request->name;
        $uv->save();
        Toastr::success('Successfully added','Success',["positionClass" => "toast-top-center"]);
        return redirect()->back();
    }
    public function destroy($id){
        $uv = Degree::findOrFail($id);
        if ($uv->educations->count() <1){
            $uv->delete();
            Toastr::success('Successfully delete','Success');
            return redirect()->back();
        }else{
            Toastr::error('This university is already use cant delete this','Error');
            return redirect()->back();
        }
    }
}
