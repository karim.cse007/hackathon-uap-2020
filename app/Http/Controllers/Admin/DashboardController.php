<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Http\Controllers\Controller;
use App\User;
use App\Work;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
     public function index()
    {
        $researcher =User::where('role_id',3)->count();
        $cmp =Company::all()->count();
        $work =Work::all()->count();
        $info=[
          'rs'=>$researcher,
            'cmp'=>$cmp,
            'work'=>$work
        ];
        return view('admin.dashboard',compact('info'));
    }
}
