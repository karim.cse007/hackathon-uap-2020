<?php

namespace App\Http\Controllers\User;

use App\Bid;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BidController extends Controller
{
    public function store(Request $request){
        $val=Validator::make($request->all(),[
           'work'=>'integer|required|not_in:0|exists:works,id',
           'bid'=>'string|required',
        ]);
        if ($val->fails()){
            return response()->json('Invalid Request',200);
        }
        if (auth()->user()->bids->where('work_id',$request->work)->first()){
            return response()->json('You already bid this!!!',200);
        }
        $bid = new Bid();
        $bid->user_id=auth()->id();
        $bid->work_id=$request->work;
        $bid->description=$request->bid;
        $bid->save();
        return response()->json('Thank you for biding',200);

    }
}
