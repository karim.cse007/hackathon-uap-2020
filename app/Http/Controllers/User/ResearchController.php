<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResearchController extends Controller
{
    public function index()

    {
        return view('user.research.research');
    }

    public function show()

    {
        return view('user.research.researchshow');
    }

    public function detail()

    {
        return view('user.research.researchdetail');
    }
}
