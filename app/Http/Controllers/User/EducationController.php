<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EducationController extends Controller
{
    public function index()

    {
        return view('user.education.education');
    }

     public function show()

    {
        return view('user.education.edushow');
    }
    public function detail()

    {
        return view('user.education.educationdetail');
    }
}
