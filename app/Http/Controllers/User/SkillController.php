<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\WorkField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SkillController extends Controller
{
    public function index()
    {
        return view('user.skill.skill');
    }

     public function show()

    {
        return view('user.skill.skillshow');
    }

    public function detail()

    {
        $allSkill = WorkField::all();
        $skills = auth()->user()->skills;
        //dd($skills);
        return view('user.skill.skilldetail',compact('skills','allSkill'));
    }
    public function store(Request $request){
        $val = Validator::make($request->all(),[
           'skills'=>'required|integer',
        ]);

        auth()->user()->skills()->sync($request->skills);
        //$skills->save();
        return response()->json('Success fully added');
    }
}
