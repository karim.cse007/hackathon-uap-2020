<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AchievementController extends Controller
{
    public function index()

    {
        return view('user.achievement.achievement');
    }

    public function show()

    {
        return view('user.achievement.achievementshow');
    }

    public function detail()

    {
        return view('user.achievement.achievementdetail');
    }
}
