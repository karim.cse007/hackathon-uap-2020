<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use App\Work;
use App\WorkField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    public function index()
    {
        $works = Work::orderBy('created_at','DESC')->get();
        return view('welcome',compact('works'));
    }

     public function contact()
    {
        return view('contact');
    }

     public function company()
    {
        $companies = Company::all();
        return view('developer',compact('companies'));
    }
    public function searchCompany(Request $request){
        $val= Validator::make($request->all(),[
            'search'=>'required|string',
        ]);
        if ($val->fails()){
            return redirect()->back();
        }
        $companies = Company::where('name','like',"%$request->search%")->get();
        return view('developer',compact('companies'));
    }

     public function researcher()
    {
        $researchers = User::where('role_id',3)->paginate(8);
        return view('researcher',compact('researchers'));
    }
    public function searchResearcher(Request $request){
        $val=Validator::make($request->all(),[
           'search'=>'string|required',
        ]);
        if ($val->fails()){
            return redirect()->back();
        }

        $field = WorkField::where('name','like',"%$request->search%")->first();
        //return response()->json($field->id,200);
        if (isset($field->id)){
            $researchers=$field->users;
            return view('rsinfo',compact('researchers'));
        }
        return redirect()->back();
    }

     public function profile()
    {

        return view('researcher.profile');
    }

     public function about()
    {
        return view('about');
    }

    public function companyDetails($id){
        $company = Company::findOrFail($id);
        $works = $company->works;
        return view('company',compact('company','works'));
    }
}
