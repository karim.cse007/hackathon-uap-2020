<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Work;
use App\WorkField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    public function store(Request $request){
        $val = Validator::make($request->all(),[
            'title'=>'String|required',
            'description'=>'String|required',
            'price'=>'required',
            'fields'=>'required'
        ]);
        //dd($request);
        if ($val->fails()){
            return redirect()->back();
        }
        //dd(auth()->user()->company);
        $post = new Work();
        $post->company_id=auth()->user()->company->id;
        $post->title = $request->title;
        $post->description = $request->description;
        $post->amount=$request->price;
        $post->save();
        $post->workFields()->attach($request->fields);
        return redirect()->back()->with('success','successfully post');
    }
    public function index()
    {
        $works = Work::where('company_id',auth()->user()->company->id)->latest()->get();
        $fields = WorkField::all();
        return view('user.company.company',compact('works','fields'));
    }
    public function history(){
        $works = Work::where('company_id',auth()->user()->company->id)->get();
        return view('user.company.work',compact('works'));
    }
}
