<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    public function workFields(){
        return $this->belongsToMany(WorkField::class);
    }
    public function bids(){
        return $this->hasMany(Bid::class);
    }
}
