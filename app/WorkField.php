<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkField extends Model
{
    public function users(){
        return $this->belongsToMany(User::class);
    }
    public function works(){
        return $this->belongsToMany(Work::class);
    }
}
